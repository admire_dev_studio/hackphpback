<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('user', 'PassportController@details');

Route::post('sign_in', 'PassportController@login');
Route::post('sign_up', 'PassportController@register');

Route::post('sector_error', 'ErrorController@sendError');

Route::get('user_list', 'PassportController@users');
Route::post('add_area', 'PassportController@addArea');
Route::get('area_list', 'PassportController@getAreas');
Route::post('get_owner', 'ErrorController@getOwner');

Route::get('cron', 'ErrorController@cron');
Route::post('get_errors', 'ErrorController@getErrors');
Route::post('pusher', 'ErrorController@sendPusher');
Route::get('obj', 'ErrorController@getObjects');
