<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['name', 'email', 'password', 'token'];

    protected $hidden = ['password'];

    public $timestamps = false;

    public function areas() {
        return $this->hasMany('App\AreaController');
    }
}
