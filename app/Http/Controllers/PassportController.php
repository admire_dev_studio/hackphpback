<?php

namespace App\Http\Controllers;

use App\AreaController;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class PassportController extends Controller
{
    /**
     * @OA\Post(
     *      path="/sign_up",
     *      operationId="register",
     *      tags={"auth"},
     *      summary="Регистрация пользователя. Email должен быть уникальным.",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           "name": {"reqiured"},
     *           "email": {"reqiured"},
     *           "password": {"reqiured"}
     *       }
     *     )
     *
     *
     * API регистрации пользователя
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        $token = bcrypt('admire_'.$request->password);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'token' => $token,
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'token' => $token,
            'user' => $user
        ], 200);
    }

    /**
     * @OA\Post(
     *      path="/sign_in",
     *      operationId="login",
     *      tags={"auth"},
     *      summary="Вход пользователя по email и паролю",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           "email": {"reqiured"},
     *           "password": {"reqiured"}
     *       }
     *     )
     *
     *
     * Возвращает информацию о пользователе и токен
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required'],
            'password' => ['required']
        ]);

        if(!User::where('email', $request->email)->exists())
            abort(404);

        $user = User::where('email', $request->email)->with('areas')->first();

        if (Hash::check($request->password, $user->password)) {
            return response()->json([
                'token' => $user->token,
                'user' => $user
            ], 200);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

    /**
     * @OA\Post(
     *      path="/user",
     *      operationId="details",
     *      tags={"auth"},
     *      summary="Получение информации о пользователе",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       )
     *     )
     *
     * Получение информации о пользователе
     */
    public function details(Request $request)
    {
        return response()->json(['user' => User::where('token', $request->header('Authorization'))->with('areas')->first()], 200);
    }

    public function users(Request $request) {
        if(!User::where('token', $request->header('Authorization'))->exists())
            abort(404);

        return User::with('areas')->get();
    }

    public function addArea(Request $request) {
        $area = AreaController::create([
            'user_id' => $request->user_id,
            'left_latitude' => $request->left_latitude,
            'right_latitude' => $request->right_latitude,
            'left_longitude' => $request->left_longitude,
            'right_longitude' => $request->right_longitude,
        ]);

        return $area;
    }

    public function getAreas(Request $request) {
        return AreaController::query()
            ->whereBetween('left_latitude', [$request->query('latitude') - 2, $request->query('latitude') + 2])
            ->whereBetween('right_latitude', [$request->query('latitude') - 2, $request->query('latitude') + 2])
            ->whereBetween('left_longitude', [$request->query('longitude') - 2, $request->query('longitude') + 2])
            ->whereBetween('right_longitude', [$request->query('longitude') - 2, $request->query('longitude') + 2])
            ->join('users', 'area_controllers.user_id', '=', 'users.id')->get()->toJson();
    }
}
