<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Hack API description",
 *      description="Hack Univercity Laravel Back API",
 *      @OA\Contact(
 *          email="alexbidenko1998@gmail.com"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */

/**
 *  @OA\Server(
 *      url="http://php-back.admire.social/api",
 *      description="L5 Swagger OpenApi Server"
 * )
 */

/**
 * @OA\Tag(
 *     name="error",
 *     description="Api for dynamic error sender messages"
 * )
 */

/**
 * @OA\Tag(
 *     name="auth",
 *     description="Апи для авторизации пользователей"
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
