<?php

namespace App\Http\Controllers;

use App\AreaController;
use App\Events\SectorError;
use App\NodeError;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;
use Pusher\PusherException;

class ErrorController extends Controller
{

    /**
     * @OA\Post(
     *      path="/sector_error",
     *      operationId="sendError",
     *      tags={"error"},
     *      summary="Отправка сообщения по соккет соединению на фронт на подписчиков ошибок для конкретного сектора",
     *      description="Отправляет сообщение и возвращает расчитанный сетор для отправки",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="latitude",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="longitude",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="node",
     *                      type="object"
     *                  ),
     *                  example={"latitude": 37.8, "longitude": 44.99, "node": {}}
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           "latitude": {"required"},
     *           "longitude": {"required"}
     *       }
     *     )
     *
     *
     * Отправляет сообщение и возвращает расчитанный сетор для отправки
     */
    function sendError(Request $request) {
        $request->validate([
            'latitude' => ['required', 'numeric'],
            'longitude' => ['required', 'numeric'],
        ]);

        $sector = [(int) ($request->latitude - $request->latitude % 2), (int) ($request->longitude - $request->longitude % 2)];

        event(new SectorError($request->all(), $sector));

        return response()->json($sector);
    }

    function getOwner(Request $request) {
        return AreaController::query()->where('left_latitude', '<', $request->latitude)
            ->where('right_latitude', '>', $request->latitude)
            ->where('left_longitude', '<', $request->longitude)
            ->where('right_longitude', '>', $request->longitude)
            ->join('users', 'area_controllers.user_id', '=', 'users.id')->first();
    }

    function cron() {
        $result = json_decode(file_get_contents('https://python-back.admire.social/water'), true)['response'];

        $er = [];

        foreach ($result as $error) {
                $error['node_id'] = $error['id'];
                $error['timestamp'] = time() * 1000;

                $res = null;

                if ($error['type'] == 'hran') {
//                    NodeError::create([
//                        'node_id' => $error['node_id'],
//                        'loses' => $error['loses'],
//                        'lat' => $error['lat'],
//                        'long' => $error['long'],
//                        'hran_result' => $error['hran_result'],
//                        'timestamp' => $error['timestamp'],
//                    ]);
                    $res = $error['hran_result'];
                } elseif ($error['type'] == 'vent') {
//                    NodeError::create([
//                        'node_id' => $error['node_id'],
//                        'loses' => $error['vent_loses'],
//                        'lat' => $error['lat'],
//                        'long' => $error['long'],
//                        'hran_result' => $error['vent_result'],
//                        'timestamp' => $error['timestamp'],
//                    ]);
                    $res = $error['vent_result'];
                }

            if ($res == 'bad') {
                $sector = [(int) ($error['lat'] - $error['lat'] % 2), (int) ($error['long'] - $error['long'] % 2)];
//                        event(new SectorError($error, $sector));
//                        $url = 'https://api-eu.pusher.com/apps/966947/events?body_md5=2c99321eeba901356c4c7998da9be9e0&auth_version=1.0&auth_key=8da04f0e1ecfefbeaecc&auth_timestamp='.time().'&auth_signature=ec6c62a7c8a96c548285851ec340605e241cfdcbdf385b1ec898828b9424f687&';
//                        $data = array('data' => json_encode(['error' => $error, 'sector' => $sector]), 'name' => 'sector-'.$sector[0].'-'.$sector[1], 'channel' => 'error-listener');
//
//                        $options = array(
//                            'http' => array(
//                                'header'  => "Content-type: application/json\r\n",
//                                'method'  => 'POST',
//                                'content' => http_build_query($data)
//                            )
//                        );
//
//                        $context  = stream_context_create($options);
//                        file_get_contents($url, false, $context);

//                $datas = array('data' => json_encode(['error' => $error, 'sector' => $sector]), 'name' => 'sector-'.$sector[0].'-'.$sector[1], 'channel' => 'error-listener');

//                $ch = curl_init();
//
//                curl_setopt($ch, CURLOPT_URL, 'https://api-eu.pusher.com/apps/966947/events?body_md5=2c99321eeba901356c4c7998da9be9e0&auth_version=1.0&auth_key=8da04f0e1ecfefbeaecc&auth_timestamp='.time().'&auth_signature=bdbd61d9cb5663234cece1ecea862dae6330d40c72fbf3f498d38692bf2c54d4&');
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch, CURLOPT_POST, 1);
//                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datas));
//
//                $headers = array();
//                $headers[] = 'Content-Type: application/json';
//                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//
//                $datas['requist'] = curl_exec($ch);
//                if (curl_errno($ch)) {
//                    return 'Error:' . curl_error($ch);
//                }
//                curl_close($ch);

                $options = array(
                    'cluster' => 'eu',
                    'useTLS' => true
                );
                try {
                    $pusher = new Pusher(
                        '8da04f0e1ecfefbeaecc',
                        '7d92e3ac99cd7e9e6b3f',
                        '966947',
                        $options
                    );

                    $pusher->trigger('error-listener', 'sector-'.$sector[0].'-'.$sector[1], $error);
                } catch (PusherException $e) {
                }

                $er[] = $error;
            }
        }

        return [$er, $result];
    }

    function sendPusher(Request $request) {
        event(new SectorError($request->error, $request->sector));
        return 0;
    }

    function getErrors(Request $request) {
        return DB::table('node_errors')->whereBetween('lat', [$request->latitude - 2, $request->latitude + 2])
            ->whereBetween('long', [$request->longitude - 2, $request->longitude + 2])
            ->groupBy('node_id')->latest('timestamp')->get();
    }

    function getObjects() {
        return file_get_contents('https://python-back.admire.social/obj');
    }
}
