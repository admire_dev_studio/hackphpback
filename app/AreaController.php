<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaController extends Model
{
    protected $fillable = ['user_id', 'left_latitude', 'right_latitude', 'left_longitude', 'right_longitude'];

    public $timestamps = false;
}
