<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NodeError extends Model
{
    protected $fillable = ['loses', 'hran_result', 'node_id', 'timestamp', 'lat', 'long'];

    public $timestamps = false;
}
