<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SectorError implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $error;
    public $sector;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($error, $sector)
    {
        $this->error = $error;
        $this->sector = $sector;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['error-listener'];
    }

    public function broadcastAs()
    {
        return 'sector-'.$this->sector[0].'-'.$this->sector[1];
    }
}
